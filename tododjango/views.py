from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Tarefas
from .serializers import ToDoSerializer
class TarefasView(APIView):    
    def get(self, request):
        todos = Tarefas.objects.all()
        todos.sort(key=lambda x: x.ordenador, reverse=True)
        serializer = ToDoSerializer(todos, many=True)
        return Response(serializer.data)
    def post(self, request):
        todos = Tarefas.objects.all()
        todos.sort(key=lambda x: x.ordenador, reverse=True)
        ultimo = todos[-1].ordenador
        item = ToDoSerializer(data=request.data)
        persistido = Todo.objects.create(finalizada=false,ordenador=ultimo,texto=item.texto)
        todos.append(persistido)
        serializer = ToDoSerializer(todos, many=True)
        return Response(serializer.data)
    def put(self, request):
        itens = ToDoSerializer(data=request.data)
        for idx, val in enumerate(itens):
            tarefa= Todo.objects.get(pk=val.id)
            tarefa.ordenador = idx
            tarefa.save()
        todos = Tarefas.objects.all()
        todos.sort(key=lambda x: x.ordenador, reverse=True)
        serializer = ToDoSerializer(todos, many=True)
        return Response(serializer.data)
    def delete(self, request, pk):
        tarefa= Todo.objects.get(pk=pk)
        tarefa.delete()
        todos = Tarefas.objects.all()
        todos.sort(key=lambda x: x.ordenador, reverse=True)
        serializer = ToDoSerializer(todos, many=True)
        return Response(serializer.data)