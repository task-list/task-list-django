from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include


from .views import TarefasView

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^tarefas/$', TarefasView.as_view()),
    url(r'^tarefas/(?P<pk>[0-9]+)/$',TarefasView.as_view()),
]